from flask import *
from flask_sqlalchemy import *
import sqlalchemy
from sqlalchemy.sql.expression import func

app =Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI']= 'mysql://root:cool@localhost/expense_mgmt'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']= True
db = SQLAlchemy(app)
DATABASE='expense_mgmt'

class Expenses(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=False)
    email = db.Column(db.String(120), unique=False)
    category = db.Column(db.String(120), unique=False)
    description = db.Column(db.String(120), unique=False)
    link = db.Column(db.String(200), unique=False)
    estimated_costs = db.Column(db.String(200), unique=False)
    submit_date = db.Column(db.String(80), unique=False)
    status = db.Column(db.String(120), unique=False)
    decision_date = db.Column(db.String(80), unique=False)

    def __init__(self, name, email, category, description, link, estimated_costs,
    submit_date, status, decision_date):
        self.name = name
        self.email = email
        self.category = category
        self.description = description
        self.link = link
        self.estimated_costs = estimated_costs
        self.submit_date = submit_date
        self.status = status
        self.decision_date = decision_date

class CreateDB():
	def __init__(self):
		import sqlalchemy
		engine = sqlalchemy.create_engine('mysql://%s:%s@%s'%("root", "cool", "localhost")) # connect to server
		engine.execute("CREATE DATABASE IF NOT EXISTS %s "%("expense_mgmt")) #create db

import redis
from flask import Flask, jsonify, request, json, Response
from flask import Flask
from flask_sqlalchemy import *

app = Flask(__name__)

@app.route('/v1/expenses', methods=['GET'])
def getExpenses():
    res_full = []
    res_full = Expenses.query.all()
    results =[]
    for res in res_full:
        result_get = {
        'id' : res.id,
        'name' : res.name,
        'email' : res.email,
        'category' : res.category,
        'description' : res.description,
        'link' : res.link,
        'estimated_costs' : res.estimated_costs,
        'submit_date' : res.submit_date,
        'status' : res.status,
        'decision_date' : res.decision_date
        }
        results.append(result_get)
    
    resp = jsonify(results)
    #resp.status_code = 200
    return resp, 200

@app.route('/v1/expenses/<string:id>', methods=['GET'])
def getExpense(id):
    res = Expenses.query.filter_by(id=id).first_or_404()
    res_get_id = {
    'id' : res.id,
    'name' : res.name,
    'email' : res.email,
    'category' : res.category,
    'description' : res.description,
    'link' : res.link,
    'estimated_costs' : res.estimated_costs,
    'submit_date' : res.submit_date,
    'status' : res.status,
    'decision_date' : res.decision_date
    }
    
    resp = jsonify(res_get_id)
    #resp.status_code = 200
    return resp, 200
    
@app.route('/v1/expenses', methods=['POST'])
def addExpense():
    CreateDB()
    db.create_all()
    requestjson=request.get_json(force=True)
    expense = Expenses(requestjson['name'],requestjson['email'],
    requestjson['category'],requestjson['description'],requestjson['link']
    ,requestjson['estimated_costs'],requestjson['submit_date'],
    'pending','09-10-2016')

    db.session.add(expense)
    db.session.commit()

    id_res = db.session.query(func.max(Expenses.id))
    res = Expenses.query.filter_by(id=id_res).first()
    result_query = {
    'id' : res.id,
    'name' : res.name,
    'email' : res.email,
    'category' : res.category,
    'description' : res.description,
    'link' : res.link,
    'estimated_costs' : res.estimated_costs,
    'submit_date' : res.submit_date,
    'status' : res.status,
    'decision_date' : res.decision_date
    }

    resp = jsonify(result_query)
    return resp, 201

@app.route('/v1/expenses/<string:id>', methods=['PUT'])
def updateExpense(id):
    
    res = Expenses.query.filter_by(id=id).first()
    requestjson=request.get_json(force=True)
    res.estimated_costs = requestjson['estimated_costs']
    
    db.session.commit()
    
    update_query= {
    'id' : res.id,
    'name' : res.name,
    'email' : res.email,
    'category' : res.category,
    'description' : res.description,
    'link' : res.link,
    'estimated_costs' : res.estimated_costs,
    'submit_date' : res.submit_date,
    'status' : res.status,
    'decision_date' : res.decision_date
    }

    resp = jsonify(update_query)
    return resp, 202

@app.route('/v1/expenses/<string:id>', methods=['DELETE'])
def deleteExpense(id):
    
    res = Expenses.query.filter_by(id=id).first_or_404()
    db.session.delete(res)
    db.session.commit()
    resp = Response(status=204, mimetype='application/json')
    return resp

if __name__ == "__main__":
    if __name__ == "__main__":
        r = redis.StrictRedis(host='127.0.0.1', port=6379, db=0)
        r.set(2, '5002')
        app.run(debug=True, host='0.0.0.0', port=5002)
        print r.get


